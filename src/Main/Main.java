package Main;

import javax.xml.ws.*;

import DAO.DAOFoods;
import WS.NewsImplements;
import WS.ProductsImplements;

public class Main {

	public static void main(String[] args) {

		int port = 4835;
		
		try {
			Endpoint.publish("http://127.0.0.1:" + port + "/ws/food_news", new NewsImplements());
			Endpoint.publish("http://127.0.0.1:" + port + "/ws/food_products", new ProductsImplements());
			System.out.println("Done");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
