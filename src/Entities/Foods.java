package Entities;

import java.util.List;

public class Foods {

	private int uid;
	private String name;
	private double rank;
	private double price;
	private int num_sales;
	private String jsonDays;

	public Foods(int uid, String name, double rank, double price, int num_sales, String jsonDays) {
		this.uid = uid;
		this.name = name;
		this.rank = rank;
		this.price = price;
		this.num_sales = num_sales;
		this.jsonDays = jsonDays;
	}

	public Foods() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRank() {
		return rank;
	}

	public void setRank(double rank) {
		this.rank = rank;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getNum_sales() {
		return num_sales;
	}

	public void setNum_sales(int num_sales) {
		this.num_sales = num_sales;
	}

	public int getUid() {
		return uid;
	}

	public String getJsonDays() {
		return jsonDays;
	}

	public void setJsonDays(String jsonDays) {
		this.jsonDays = jsonDays;
	}

	public String toString() {
		return "Foods [uid=" + uid + ", name=" + name + ", rank=" + rank + ", price=" + price + ", num_sales="
				+ num_sales + ", jsonDays=" + jsonDays + "]";
	}

	
}
