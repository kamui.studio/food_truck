package Entities;

public class News {

	private int uid;
	private String pubdate;
	private String title;
	private String content;
	private String image;
	private String permalink;

	public News(int uid, String title, String content, String pubdate, String image, String permalink) {
		this.uid = uid;
		this.pubdate = pubdate;
		this.title = title;
		this.content = content;
		this.image = image;
		this.permalink = permalink;
	}

	public News() {
	}

	public String getPubdate() {
		return pubdate;
	}

	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public int getUid() {
		return uid;
	}

	@Override
	public String toString() {
		return "News [uid=" + uid + ", pubdate=" + pubdate + ", title=" + title + ", content="
				+ content + ", image=" + image + ", permalink=" + permalink + "]";
	}
		
}
