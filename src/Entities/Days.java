package Entities;

public class Days {

	private int uid;

	public Days(int uid) {
		this.uid = uid;
	}

	public Days() {
	}

	public int getUid() {
		return uid;
	}

	public String toString() {
		return "Days [uid=" + uid + "]";
	}
	
}
