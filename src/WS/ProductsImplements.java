package WS;

import java.util.List;
import javax.jws.*;
import DAO.DAOFoods;
import Entities.Foods;

@WebService(endpointInterface = "WS.IProduct")
public class ProductsImplements implements IProduct {

	private DAOFoods d = new DAOFoods();
	
	public Foods findProdById(int pid) {
		return d.findById(pid);
	}

	public List<Foods> findAll() {
		return d.findAll();
	}

	public List<Foods> findBest(int max) {
		return d.findBest(max);
	}

	public List<Foods> findByString(String str) {
		return d.findByString(str);
	}

	public List<Foods> findAdvanced(String qString, String qDate, int qCat) {
		return d.findAdvanced(qString, qDate, qCat);
	}
	
}