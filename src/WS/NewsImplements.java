package WS;

import java.util.List;
import javax.jws.*;
import DAO.DAONews;
import Entities.News;

@WebService(endpointInterface = "WS.INews")
public class NewsImplements implements INews {

	private DAONews d = new DAONews();
	
	public News findNewsById(int uid) {
		return d.findById(uid);
	}

	public List<News> findLast(int nws) {
		return d.findLast(nws);
	}
	
}