package WS;

import java.util.List;
import javax.jws.*;
import Entities.News;

@WebService
public interface INews {

	@WebMethod
	public News findNewsById(int uid);
	
	@WebMethod
	public List<News> findLast(int nws);
	
}
