package WS;

import java.util.List;
import javax.jws.*;
import Entities.Foods;

@WebService
public interface IProduct {

	@WebMethod
	public Foods findProdById(int pid);
	
	@WebMethod
	public List<Foods> findAll();

	@WebMethod
	public List<Foods> findBest(int max);
	
	@WebMethod
	public List<Foods> findByString(String str);
	
	@WebMethod
	public List<Foods> findAdvanced(String qString, String qDate, int qCat);
}
