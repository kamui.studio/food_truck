package DAO;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T,K> {

	void create(T obj) throws ClassNotFoundException, SQLException;
	T findById(K uid)  throws ClassNotFoundException, SQLException;
	List<T> findAll()  throws ClassNotFoundException, SQLException;
	void update(T obj) throws ClassNotFoundException, SQLException;
	void delete(K uid) throws ClassNotFoundException, SQLException;

}