package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import Entities.News;

public class DAONews implements DAO<News, Integer> {

	static private String DBHOST = "jdbc:mysql://127.0.0.1/AJC";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	static private String PREFIX = "ws_food_";
	
	// []

	public void create(News n) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `" + PREFIX + "students` (`id_house`, `last_name`, `first_name`, `avatar`, `age`) VALUES (?, ?, ?, ?, ?);");
			ps.setInt(1, s.getId_house());
			ps.setString(2, s.getLast_name());
			ps.setString(3, s.getFirst_name());
			ps.setString(4, s.getAvatar());
			ps.setInt(5, s.getAge());
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/

	}

	// []
	public News findById(Integer uid) {

		News n = new News();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT `" + PREFIX + "news`.* FROM `" + PREFIX + "news` WHERE `" + PREFIX + "news`.`id`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				n = new News(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getString("pubdate"), rs.getString("image"), rs.getString("permalink"));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return n;

	}

	// []
	public List<News> findLast(Integer max) {

		List<News> News = new ArrayList();
		if (max > 5) max = 5;
		else if (max == 0) max = 3;
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT `" + PREFIX + "news`.* FROM `" + PREFIX + "news` WHERE 1 ORDER BY `" + PREFIX + "news`.`pubdate` DESC LIMIT 0,?");
			ps.setInt(1, max);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				News.add(new News(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getString("pubdate"), rs.getString("image"), rs.getString("permalink")));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return News;

	}

	// []
	public List<News> findAll() {

		List<News> News = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `" + PREFIX + "news` WHERE 1");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				News.add(new News(rs.getInt("id"), rs.getString("title"), rs.getString("content"), rs.getString("pubdate"), rs.getString("image"), rs.getString("permalink")));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return News;
	}

	// []
	public void update(News n) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `" + PREFIX + "students` SET `first_name`=?, `last_name`=?, `age`=? WHERE 1 AND `id`=?");
			ps.setString(1, c.getFirst_name());
			ps.setString(2, c.getLast_name());
			ps.setInt(3, c.getAge());
			ps.setInt(4, c.getId());
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
		
	}

	// []
	public void delete(Integer uid) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `" + PREFIX + "students` WHERE `" + PREFIX + "students`.`id`=?");
			ps.setInt(1, uid);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
	
	}



}