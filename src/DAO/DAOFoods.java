package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.google.gson.Gson;

import Entities.Days;
import Entities.Foods;

public class DAOFoods implements DAO<Foods, Integer> {

	static private String DBHOST = "jdbc:mysql://127.0.0.1/AJC";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	static private String PREFIX = "ws_food_";
	
	// []

	public void create(Foods p) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `" + PREFIX + "students` (`id_house`, `last_name`, `first_name`, `avatar`, `age`) VALUES (?, ?, ?, ?, ?);");
			ps.setInt(1, s.getId_house());
			ps.setString(2, s.getLast_name());
			ps.setString(3, s.getFirst_name());
			ps.setString(4, s.getAvatar());
			ps.setInt(5, s.getAge());
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/

	}

	public List<Days> findDays(Integer uid) {
		
		List<Days> d = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT `week_day` FROM `" + PREFIX + "products_has_days` WHERE 1 AND `" + PREFIX + "products_has_days`.`id_food_products`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				d.add(new Days(rs.getInt("week_day")));

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return d;
	}
	
	
	// []
	public Foods findById(Integer uid) {

		Foods p = new Foods();
		List<Days>  Days = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT `" + PREFIX + "products`.* FROM `" + PREFIX + "products` WHERE `" + PREFIX + "products`.`id`=?");
			ps.setInt(1, uid);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Days = findDays(rs.getInt("id"));

				Gson gson = new Gson();
				String json = gson.toJson(Days);
				String strjson = json.toString();
				
				p = new Foods(rs.getInt("id"), rs.getString("name"), rs.getDouble("ranking"), rs.getDouble("price"), rs.getInt("num_sales"), strjson);
			}

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return p;

	}
	

	// []
	public List<Foods> findAll() {

		List<Foods> Products = new ArrayList();
		List<Days>  Days = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `" + PREFIX + "products` WHERE 1");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Days = findDays(rs.getInt("id"));
				
				Gson gson = new Gson();
				String json = gson.toJson(Days);
				String strjson = json.toString();

				Products.add(new Foods(rs.getInt("id"), rs.getString("name"), rs.getDouble("ranking"), rs.getDouble("price"), rs.getInt("num_sales"), strjson));
			}

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return Products;
	}

	// []
	public List<Foods> findByString(String str) {

		List<Foods> Products = new ArrayList();
		List<Days>  Days = new ArrayList();
		String whr = "`" + PREFIX + "products`.`name` like '%" + str + "%'";

		// Check if there is more than one string to search
		String[] q = str.split(" ");
		if (q.length > 1) {
			String cls = "";
			for (int i = 0; i < q.length; i++)
				cls += (cls.equals("") ? "" : " OR ") + "`" + PREFIX + "products`.`name` like '%" + q[i] + "%'";
			whr = "(" + cls + ")";
		}
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `" + PREFIX + "products` WHERE 1 AND " + whr);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Days = findDays(rs.getInt("id"));

				Gson gson = new Gson();
				String json = gson.toJson(Days);
				String strjson = json.toString();
				
				Products.add(new Foods(rs.getInt("id"), rs.getString("name"), rs.getDouble("ranking"), rs.getDouble("price"), rs.getInt("num_sales"), strjson));
			}

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return Products;
	}

	// []
	public List<Foods> findAdvanced(String qString, String qDate, int qCat) {

		List<Foods> Products = new ArrayList();
		List<Days>  Days = new ArrayList();
		String whr = "", grp = "", tab = "`" + PREFIX + "products`";
		
		// Check if there are matching dates
		if (!qDate.equals("")) {
			tab += (tab.equals("") ? "" : ", ") + "`" + PREFIX + "products_has_days`";
			whr += " AND `" + PREFIX + "products_has_days`.`week_day` = DAYOFWEEK('" + qDate + "') AND `" + PREFIX + "products_has_days`.`id_food_products` = `" + PREFIX + "products`.`id`";
			grp = " GROUP BY `" + PREFIX + "products`.`id`";
		}

		// Check there are matching strings
		if (!qString.equals("")) {
	
			// Check if there is more than one string to search
			String[] q = qString.split(" ");
			if (q.length > 1) {
				String cls = "";
				for (int i = 0; i < q.length; i++)
					cls += (cls.equals("") ? "" : " OR ") + "`" + PREFIX + "products`.`name` like '%" + q[i] + "%'";
				whr += " AND (" + cls + ")";
			} else 
				whr += " AND `" + PREFIX + "products`.`name` like '%" + qString + "%'";

		}

		// Check for matching categories
		if (qCat != 0) {
			tab += (tab.equals("") ? "" : ", ") + "`" + PREFIX + "products_has_categories`";
			whr += " AND `" + PREFIX + "products_has_categories`.`id_categories` = '" + qCat+ "' AND `" + PREFIX + "products_has_categories`.`id_food_products` = `" + PREFIX + "products`.`id`";
			grp = " GROUP BY `" + PREFIX + "products`.`id`";			
		}
		

		
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("SELECT `" + PREFIX + "products`.* FROM " + tab + " WHERE 1 " + whr + grp);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Days = findDays(rs.getInt("id"));

				Gson gson = new Gson();
				String json = gson.toJson(Days);
				String strjson = json.toString();
				
				Products.add(new Foods(rs.getInt("id"), rs.getString("name"), rs.getDouble("ranking"), rs.getDouble("price"), rs.getInt("num_sales"), strjson));
			}

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return Products;
	}

	// []
	public List<Foods> findBest(int lim) {

		List<Foods> Products = new ArrayList();
		List<Days>  Days = new ArrayList();
		
		// Mise en place de la limite par d�faut
		if (lim > 5) lim = 5;	// Limit max 5
		else if (lim == 0) lim = 3; 
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `" + PREFIX + "products` WHERE 1 ORDER BY `" + PREFIX + "products`.`num_sales` DESC LIMIT 0,?");
			ps.setInt(1, lim);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Days = findDays(rs.getInt("id"));

				Gson gson = new Gson();
				String json = gson.toJson(Days);
				String strjson = json.toString();
				
				Products.add(new Foods(rs.getInt("id"), rs.getString("name"), rs.getDouble("ranking"), rs.getDouble("price"), rs.getInt("num_sales"), strjson));
			}
				

			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return Products;
	}



	// []
	public void update(Foods p) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `" + PREFIX + "students` SET `first_name`=?, `last_name`=?, `age`=? WHERE 1 AND `id`=?");
			ps.setString(1, c.getFirst_name());
			ps.setString(2, c.getLast_name());
			ps.setInt(3, c.getAge());
			ps.setInt(4, c.getId());
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
		
	}


	// []
	public void delete(Integer uid) {

		/*
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `" + PREFIX + "students` WHERE `" + PREFIX + "students`.`id`=?");
			ps.setInt(1, uid);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		*/
	
	}

}